Pod::Spec.new do |s|
	s.name = 'GoSquared'
	s.version = '0.1.0'
	s.summary = 'Unofficial GoSquared native SDK for iOS'
	s.homepage = 'https://bitbucket.org/urbanmassage/gosquared-ios-native'
	s.license = 'MIT'
	s.author = { 'Giles Williams' => 'giles.williams@gmail.com' }

	s.source = { git: 'https://bitbucket.org/urbanmassage/gosquared-ios-native.git', tag: "v#{s.version}" }

	s.requires_arc = true
	s.ios.deployment_target = '6.0'

	s.source_files = '*.{h,m}'
	s.public_header_files = '*.h'

	s.frameworks = 'Foundation', 'UIKit'
end
